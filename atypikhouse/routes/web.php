<?php
use Illuminate\Http\Request;


Route::get('/', 'homeController@index');

Route::get('/habitation', 'habitationController@showHabitation')->middleware('auth');
//Route::post('/store', 'habitationController@storeMedia');
Route::get('image/upload','habitationController@fileCreate');
Route::post('/presentation','habitationController@presentation');
Route::get('/presentation','habitationController@presentation');
Route::get('/habitation/{id}','habitationController@detailHabitation');
Route::post('/detailReseravation','habitationController@detailReseravation')->name("detailReseravation")->middleware('auth');

Route::post('/search','searchController@searchHabitation');
Route::get('/search','searchController@searchHabitation');
Route::get('/searchtop','searchController@searchTop');
Route::get('/searchHome','searchController@searchHome');
Route::post('/searchHome','searchController@searchHome');
Route::post('image/upload/store','habitationController@fileStore');
Route::post('image/delete','habitationController@fileDestroy');

Route::get('/profile','profileController@showProfile');
Route::post('/searchHome','searchController@showProfile');
Route::post('/editprofil','profileController@EditProfile')->name('edit_profil')->middleware('auth');
Route::get('/history','reservationController@index')->name('history')->middleware('auth');
Route::get('/validate','reservationController@validate_reservation')->name('validate_reservation')->middleware('auth');
Route::post('/update_reservation','reservationController@update_reservation')->name('update_reservation')->middleware('auth');
Route::get('/history_reservation','reservationController@history_reservation')->name('history_reservation')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
//->middleware('auth')

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('logout', 'Auth\LoginController@logout');
Route::get('category/{id}', 'CategoryController@show');
Route::get('/habitations','habitationController@show');
Route::post('/contact_owner','MailController@basic_email')->name('contact_owner');

Route::get( '/paiement', function (Request $request) {
    return view("paiement");
});
Route::post( '/paiement','PaiementController@pay');
Auth::routes();
