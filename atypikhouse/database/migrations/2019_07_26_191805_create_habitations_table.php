<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHabitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_categorie');
            $table->string('title_habitation');
            $table->text('desc_habitation')->nullable();
            $table->boolean('available' )->default(0);
            $table->text('adresse_habitation');
            $table->text('photos' );
            $table->text('regles' )->nullable();
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitations');
    }
}
