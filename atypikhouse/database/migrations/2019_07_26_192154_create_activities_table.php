<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_habitation');
            $table->string('title_activity' );

            $table->string('distance' )->nullable();
            $table->float('price' )->nullable();
            $table->text('photos_activity' )->nullable();
            $table->foreign('id_habitation')->references('id')->on('habitations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
