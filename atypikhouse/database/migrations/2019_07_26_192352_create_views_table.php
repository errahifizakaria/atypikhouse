<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('note')->nullable();
            $table->unsignedBigInteger('id_habitation');
            $table->unsignedBigInteger('id_comment');
            $table->foreign('id_habitation')->references('id')->on('habitations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('id_comment')->references('id')->on('comments')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('views');
    }
}
