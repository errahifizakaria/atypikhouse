<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_habitation');
            $table->string('id_equipment')->nullable();
            $table->string('rooms')->nullable();
            $table->float('price')->nullable();
            $table->string('tax')->nullable();
            //$table->text('photos_activity' );
            $table->timestamps();
            $table->foreign('id_habitation')->references('id')->on('habitations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
