@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')
<div class="container container_profile">
    <div class="control_link row">
        <div class="links_profile col-md-3">
            <a href="#" class="link_update_info">Modifier le profile </a>
            <a href="#" class="link_update_photo">Photo de profile</a>
        </div>
        <form class="col-md-7 form-horizontal" method="POST" action="{{ route('edit_profil') }}" id="regiter_form" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-pencil-alt"></i>  Profile
                </div>
                <div class="card-body">
           <div class="form_one_update">


                   <div class="form-group row">
                       <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                       <div class="col-md-6">
                           <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}"  autocomplete="name">

                           @error('name')
                           <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                           @enderror
                       </div>
                   </div>

                   <div class="form-group row">
                       <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                       <div class="col-md-6">
                           <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}"  autocomplete="email">

                           @error('email')
                           <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                           @enderror
                       </div>
                   </div>
               <div class="form-group  row">

                   <label for="birth" class="col-md-4 col-form-label text-md-right">{{ __('Date de naissance ') }}</label>

                   <div class="col-md-6">
                       <input id="birth" type="text" class="form-control  @error('birth') is-invalid @enderror" name="birth" value="{{ $user->birth }}"  autocomplete="birth" placeholder="Date de naissance">

                       @error('birth')
                       <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                       @enderror
                   </div>
               </div>
                   <div class="form-group row">
                       <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                       <div class="col-md-6">
                           <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                           @error('password')
                           <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                           @enderror
                       </div>
                   </div>

                   <div class="form-group row">
                       <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                       <div class="col-md-6">
                           <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                       </div>
                   </div>


           </div>
           <div class="form_two_update">
               <div class="profile-header-img">

                   <img class="rounded-circle" src="/storage/users/{{ $user->avatar }}" />

               </div>
               <div class="container_file">
                   @error('avatar')
                   <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                   @enderror
                   <span>Photo  </span><input type="file" id="gallery-photo-add-user" name="avatar" class="inputfile">
                   <label for="gallery-photo-add-user">Uploder la photo</label>
               </div>
               <div class="gallery-user">
                   <div class="container_img"><div class="close_img"></div></div>


               </div>
           </div>
            <div class="form-group row ">
                <div class="col-md-12 ">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Enregistrer') }}
                    </button>
                </div>
            </div>
                </div>
                </div>
        </form>


    </div>
</div>
@stop