@extends('template.master')

@section('title', 'Atypikhouse')
@section('style')
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    @stop

    </head>
    <body>


    @section('content')


        <div class="container_detail_habitation ">
            <?php $photos = explode(',', $habitation->photos); ?>
            <div class="cover_habitation" style="background-image: url({{ asset('/images/photos_habitation/'.$photos[0]) }});">
                <div class="container_img_habitation">
                    @foreach($photos as $key => $photo)

                        <a href="{{ asset('/images/photos_habitation/'.trim($photo)) }}" data-fancybox="gallery"
                           data-caption="{{$habitation->title_habitation}} photo {{$key + 1}}">
                            <img src="{{ asset('/images/photos_habitation/'.trim($photo)) }}" alt=""/>
                        </a>

                    @endforeach
                </div>
            </div>
            <div class="container content_habitation" data-id="{{$habitation->id}}">
                <div class="row">
                    <div class=" col-md-8 ">
                        <div class="container_hedear_title">
                        <h1 class="title_habitation">{{$habitation->title_habitation}}</h1>

                            <div class="info_owner">
                                <span>{{$owner->name}}</span> <img class="rounded-circle"  src="/storage/{{$owner->avatar}}" alt="{{$owner->name}}">
                            </div>




                        </div>
                        <div class="desc_habitation">
                            <p>
                                {{$habitation->desc_habitation}}
                            </p>
                        </div>
                        <h3>Équipements</h3>
                        <div class="equipments">
                            <ul class="equipments_list">
                                @foreach($equipments as $key => $equipment)
                                    <li><img src="/storage/{{$equipment->icone}}" alt=""><span class="title_equip">{{$equipment->name}}</span></li>
                                @endforeach
                            </ul>
                        </div>
                        <h3>Lieu</h3>
                        <div class="adresse_habitation">
                            <p> {{$habitation->adresse_habitation}}</p>
                        </div>
                        <h3>Réglement & condition</h3>
                        <div class="regles">
                            <p>{{$habitation->regles}}</p>
                        </div>
                        <h3>Disponibilité</h3>
                        <div class="regles">
                            <p><span>Du</span>

                                {{date('d/m/Y', strtotime(strtr($disponiblite->date_debut, '-', '/')))}}<span> à </span> {{date('d/m/Y', strtotime(strtr($disponiblite->date_fin, '-', '/')))}}  </p>


                        </div>

                    </div>
                    <div class="col-md-4">
                            <div class="price_reservation">
                               <span> {{$featured->first()->price}}€</span> par nuit

                            </div>

                        <div class="card">
                            <div class="card-header text-center">
                                <i class="fas fa-pencil-alt"></i>  Réservation
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{route('detailReseravation')}}" id="reservation_habiation">
                                    @csrf
                                    <p>Ajoutez des dates pour afficher calculer le prix</p>

                                    <div class="form-group flatpickr_reservation ">
                                        <a class="input-button" title="toggle" data-toggle>
                                            <img src="{{asset('/img/date.svg')}}" alt="date icon" width="16" height="16" class="date_icon">
                                        </a>
                                        <input type="text" placeholder="Arrivée - Départ " data-input class="text-center form-control col-12" name="date_reservation" id="date_reservation">


                                    </div>
                                    <div class="form-group ">
                                        <select id="nombre_location" class="form-control" name="nombre_voyageur">
                                            <option selected>voyageurs</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="mini_cart">
                                            <div class="cart_row">
                                            <span class="price_cart">  {{$featured->first()->price}} </span> <span></span>€ x &nbsp;<span class="days_reservation"> 1 &nbsp; </span> nuits
                                            </div>
                                            <div class="cart_row">
                                                <div class="total_cart">
                                                    <span>Total  </span>  <span class="price_cart_total"> {{$featured->first()->price}} </span>€
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary submit_reservation_habiation" id="submit_reservation_habiation">Réserver</button>



                                    <small id="contact" class="form-text text-muted text-center"><a href="#" class="form-text text-muted" data-toggle="modal" data-target="#contact_owner"> CONTACTER LE PROPRIÉTAIRE</a></small>

                                        <div class="errors_reservation">
                                            <ul>
                                                <li></li>
                                            </ul>
                                        </div>


                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row p-3">
                    <h3>Activitivé</h3>
                    <div class=" col-12" id="slick_activities">
                        @foreach ($activities as $single_activitie)

                            <div class="item_activities col-md-4 col-sm-12">
                                <a href="#"> <img src="{{asset('/images/photos_activitie/'.$single_activitie->photos_activity.'')}}" alt=""></a>
                                <div class="price_recommended">{{$single_activitie->price}}€</div>
                                <div class="title_recommended">{{$single_activitie->title_activity}}</div>
                                <div class="description_recommended">
                                    {{ \Illuminate\Support\Str::words($single_activitie->desc_activite, 12, ' ...')}}
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>


        </div>
        <div class="modal fade" id="contact_owner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Contact</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('contact_owner')}}" id="form_contact_owner" method="post">
                            @csrf
                            @guest
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" name="user_guest">
                            </div>
                            @endguest
                            <div class="form-group">
                                <label for="formGroupExampleInput">Sujet</label>
                                <input type="text" class="form-control" id="formGroupExampleInput" name="subjet" >
                            </div>
                            <div class="form-group">

                                <label for="txt_form_contact_owner">Message :</label>

                                <textarea class="form-control @error('txt_form_contact_owner') is-invalid @enderror" id="txt_form_contact_owner" rows="3" name="message_to_owner"></textarea>

                                <div class=" error_txt_form_contact_owner"></div>
                            </div>
                            <input type="hidden" value="{{$owner->email}}" name="owner_email">
                            <button type="submit" class="btn btn-primary" id="form_contact_owner_submit">Envoyé</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

@stop
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>

        $( document ).ready(function() {

                var date_begin = "{{\Carbon\Carbon::parse(strtotime(strtr($disponiblite->date_debut, '-', '/')))->format("d/m/Y")}}";
                var date_end = " {{\Carbon\Carbon::parse(strtotime(strtr($disponiblite->date_fin, '-', '/')))->format("d/m/Y")}}";
            flatpickr.localize(flatpickr.l10ns.fr);
            flatpickr(".flatpickr_reservation", {

                dateFormat: "d-m-Y",
                wrap: true,
                minDate: date_begin,
                maxDate: date_end,
                mode: 'range',


            });
        });



</script>