@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')



    <div class="recommended container">
                <h1 class="text-center"> Historique réservations </h1>
        <div class="row">
            <div class="container history_search" id="history_search">

                @if(sizeof($reservation) > 0)
                @foreach ($reservation as $row)



                    <div class="item_habitation">
                        <a href="#"> <img src="{{asset("/img/recommended.png")}}" alt=""></a>
                        <div class="price_habitation">{{$row->title_habitation}}</div>

                        <div class="description_habitation">
                            {{$row->adresse_habitation}}
                           <div> {{sizeof($reservation)}} <span>{{ __('réservation') }}</span></div>
                        </div>

                    </div>
                @endforeach
@endif



            </div>
        </div>
    </div>

@stop