<!-- Footer -->
<footer class="page-footer font-small dark_footer py-5">

<div class="container_footer container">


            <!-- Grid column -->
            <div class="item_footer ">

                <!-- Links -->
                <a class="" href="/">
                    <img src="{{ asset('/img/logo_atypikhouse_white.svg') }}"  class="d-inline-block align-top logo_" alt="">
                </a>
                <hr class="clearfix w-100 d-md-none">

            </div>
            <!-- Grid column -->



            <!-- Grid column -->
            <div class="item_footer">

                <!-- Links -->


                <ul class="list-unstyled">
                    <li>
                        <a href="#!">À propos de nous
                        </a>
                    </li>
                    <li>
                        <a href="#!">Contact
                        </a>
                    </li>
                    <li>
                        <a href="#!">Termes & Conditions</a>
                    </li>

                </ul>

            </div>


            <!-- Grid column -->
            <div class="item_footer">


                            <div class="link_footer_social">

                        <!-- Facebook -->
                        <a class="fb-ic">
                            <i class="fa fa-facebook fa-2x"></i>
                        </a>
                        <!-- Twitter -->
                        <a class="tw-ic">
                            <i class="fa fa-twitter fa-lg white-text fa-2x"> </i>
                        </a>

                        <!--Instagram-->
                        <a class="ins-ic">
                            <i class="fa fa-instagram fa-lg white-text  fa-2x"> </i>
                        </a>


                </div>

            </div>
            <!-- Grid column -->



            <!-- Grid column -->
            <div class="item_footer ">

                <!-- Links -->

                <form class="input-group">
                <input type="text" class="form-control form-control-sm" placeholder="Votre email"
                       aria-label="Your email" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-sm btn-outline-white my-0" type="button">Envoyé</button>
                </div>
                </form>

            </div>

</div>
    <div class="footer-copyright text-center ">
        Copyright © {{ date("Y")}} atypikhouse.com

    </div>
</footer>
<!-- Footer -->


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script src="{{ asset('/js/app.js') }}" defer></script>


@include('cookieConsent::index')
</div>
</body>
</html>