@include("template.header")

<div class="container-home">
    @yield('content')
</div>
@include("template.footer")
</body>
</html>