<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Styles -->
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <!--favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    @yield('style')
    @yield('scripts')

</head>
<body>

@if (\Request::is('/'))
<nav class="navbar navbar-light home ">
@else
<nav class="navbar navbar-light ">
@endif
    <a class="navbar-brand" href="/">
        <img src="{{ asset('/img/logo_atypikhouse.svg') }}"  class="d-inline-block align-top logo_" alt="">
    </a>

    @if (\Request::path() == "/" || \Request::path() == "/home" )

    @else
        <form action="{{url("/searchtop")}}" method="get" class="col-md-6 d-none d-sm-block" id="search_form">
            <div class="input-group">
                <input type="text" class="form-control" name="search_habitation" id="search_habitation" placeholder="Rechercher une habitation">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <div id="result"></div>
        </form>
    @endif
    <ul class="nav justify-content-lg-end">

        @guest

        <li class="nav-item">
            <a class="nav-link login_lick" href="{{ url("login/") }}" data-toggle="modal" data-target="#modal" class="title m-b-md" >Connexion</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  register_lick" href="{{ route('register') }}" data-toggle="modal" data-target="#registerModal">
                S'inscrire
            </a>
        </li>

        @endguest

        @auth
        @if(Auth::user()->is_owner)

        <li class="nav-item">
            <a class="nav-link " href="{{ url('/habitation') }}">Ajoutez votre établissement</a>
        </li>

        @endif
        <li class="nav-item">
            <div class="dropdown">
                <a href=""# class="account" >
                   <span>
                    {{ \Illuminate\Support\Str::words(Auth::user()->name, 1, '')}}</span>
                    <img class="rounded-circle"  src="/storage/{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}">

                </a>
                <div class="submenu" style="display: none; ">
                    <ul class="root">
                        <li >
                            <a href="{{url("/profile")}}" >Profile</a>
                        </li>
                        <li >
                            <a href="{{url("/history_reservation")}}" >Historique Réservation</a>
                        </li>
                        @if(Auth::user()->is_owner)
                        <li >
                            <a href="{{url("/validate")}}" >valider Réservation</a>
                        </li>
                        @endif
                        <li>
                         <a class="nav-link" href="{{ url("logout") }}">déconnecter</a>
                        </li>
                    </ul>
                </div>
            </div>


        </li>
        @endauth

    </ul>
</nav>
        <div id="modal" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title">Connexion</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form_custom" role="form" method="POST" action="{{ route('login') }}" id="login_form">
                            {{ csrf_field() }}
                            <div class="form-group">

                                            <div>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror

                                </div>
                            </div>

                            <div class="form-group">

                                <div>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Mot de passe">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                    <button type="submit" class="btn btn-primary col-12">
                                        Connexion
                                    </button>

                            </div>
                                <div class="form-group text-center">
                                    <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                        Mot de passe oublié?
                                    </a>
                                </div>

                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inscription</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form_custom form-horizontal" method="POST" action="{{ route('register') }}" id="regiter_form">
                            @csrf

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nom">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group  row">

                                <div class="col-md-12">
                                    <input id="birth" type="text" class="form-control  @error('email') is-invalid @enderror" name="birth" value="{{ old('birth') }}" required autocomplete="email" placeholder="Date de naissance">

                                    @error('birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Mot de passe">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmer mot de passe">
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <div class="form-check ">
                                <input class="form-check-input" type="checkbox" id="gridCheckOwner" name="isOwner">
                                <label class="form-check-label" for="gridCheckOwner">
                                    vous étes propriétaire ?
                                </label>

                                </div>
                                <div class="form group row col-md-12">
                                    <label class="form-label" for="siret">
                                        Numéro SIREN


                                    <input type="text" class="form-control" name="siret" id="siret" placeholder="123456789">
                                </div>
                            </div>
                            <div class="form-group row  col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck1">
                                    <label class="form-check-label" for="gridCheck1">
                                        veuillez cochez la case pour accepter <a href="#">les conditions générales</a> de vente du site
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-md-12 ">
                                    <button type="submit" class="btn btn-primary" id="submit_regiter">
                                        {{ __('Inscrire') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
