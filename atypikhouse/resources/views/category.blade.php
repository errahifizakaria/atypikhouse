@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')







    <div class=" container">

        <div class="row">
            @if(isset($habitation_category) && count($habitation_category) > 0)

                @foreach ($habitation_category as $row)
                    <div class="mt-4 col-md-3 col-sm-12 " id="container_category">
                        <div class="item_habitation">
                            <a href="/habitation/{{$row->id}}"> <img
                                        src="{{ asset('/images/photos_habitation/' . $row->photos . '') }}"
                                        alt="{{$row->title_habitation}}"></a>
                            <div class="title_habitation">{{$row->title_habitation}}</div>
                            <div class="price_habitation">{{$row->feature->price}} €</div>
                            <div class="description_habitation">

                                {{ \Illuminate\Support\Str::words($row->desc_habitation, 12, ' ...')}}

                            </div>
                        </div>
                    </div>
                        @endforeach

                    </div>


                        @else
                            <p class="text-center">Pas de logement pour cette category </p>
                        @endif

        </div>
    </div>

@stop