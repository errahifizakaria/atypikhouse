@extends('template.master')

@section('title', 'Atypikhouse')


</head>
<body>


@section('content')




    <div class="container_slider" style="background-image: url({{asset('/img/home_diapo.jpg')}})">
        <div class="row">
            <form class="form-inline container" action="{{url("searchHome")}}" id="search_form_home">
                {{ csrf_field() }}
                <div class="form-group col-md-2 col-xs-12">

                    <select id="type_location" class="form-control" name="type_location">
                        <option>Type location</option>
                        @foreach ($categories as $cat)
                            <option>{{$cat->title_categorie}}</option>

                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 col-xs-12 destiniation">

                    <input type="text" class="form-control col-12 typeahead" id="where" name="where"
                           placeholder="Où allez-vous ?">
                </div>
                <div class="form-group flatpickr_date col-md-3 col-xs-12">
                    <a class="input-button" title="toggle" data-toggle>
                        <img src="{{asset('/img/date.svg')}}" alt="date icon" width="16" height="16" class="date_icon">
                    </a>
                    <input type="text" placeholder="départ-arrivée" data-input class="text-center form-control col-12" name="date_reservation">


                </div>
                <div class="form-group col-md-2 col-xs-12">
                    <select id="nombre_location" class="form-control" name="nombre_voyager">
                        <option selected>voyageurs</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary col-md-2 col-xs-12">
                    Rechercher
                </button>
            </form>
        </div>

    </div>
    @if(sizeof($habitation_most_viewed) > 1)

    <div class="most_viseted container">
        <h2 class="title_custom">Destination plus visité</h2>
        <div class="row">
            <div class="slick_most_viseted container" id="slick_most_viseted">


                @foreach ($habitation_most_viewed as $single_habitation)
                    <div class="item_recommended">
                        <a href="/habitation/{{$single_habitation->id}}"> <img src="{{ asset('/images/photos_habitation/' . $single_habitation->photos. '') }}" alt=""></a>
                        <div class="title_recommended">{{$single_habitation->title_habitation}}</div>
                        <div class="price_recommended">{{$single_habitation->price}}€</div>
                        <div class="description_recommended">
                            {{ \Illuminate\Support\Str::words($single_habitation->desc_habitation, 12, ' ...')}}
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
    @endif




    @if(sizeof($last_habitation) > 0)

        <div class="recommended container">
                    <h2 class="title_custom">  Dernier Propriétés</h2>
            <div class="row">
                <div class="slick_recommended container" id="slick_properties">

                    @foreach ($last_habitation as $single_habitation)

                        <div class="item_recommended">


                            <a href="/habitation/{{$single_habitation->id}}"> <img  src="{{ asset('/images/photos_habitation/' . $single_habitation->photos. '') }}" alt=" {{$single_habitation->title_habitation}}"></a>
                            <div class="title_recommended">{{$single_habitation->title_habitation}}</div>
                            <div class="price_recommended">{{$single_habitation->feature->price}} €</div>


                            <div class="description_recommended">  {{ \Illuminate\Support\Str::words($single_habitation->desc_habitation, 12, ' ...')}}</div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
@endif

        @if(sizeof($categories) > 0)
            <div class="container">
               <h2 class="title_custom"> Les hébergements </h2>
                    <div class="bloc-wrapper">

                        @foreach ($categories as $cat)
                            <div class="container_wrapper">
                            <a href="/category/{{$cat->id}}" style="background-image: url({{asset('storage/' . $cat->img_cat. '')}})">

                            <span>{{$cat->title_categorie}}</span>
                            </a>
                            </div>
                        @endforeach
             </div>
        @endif

        @if(sizeof($habitation_recommended) > 0)
            <div class="recommended container">
                <h2 class="title_custom">Destination recommandé </h2>
                <div class="row">
                    <div class="slick_recommended container" id="slick_most_viseted">
                        data[]
                        @foreach ($habitation_recommended as $single_habitation)

                            <div class="item_recommended">
                                <a href="/habitation/{{$single_habitation->id}}"> <img src="{{ asset('/images/photos_habitation/' . $single_habitation->photos. '') }}" alt=""></a>
                                <div class="title_recommended">{{$single_habitation->title_habitation}}</div>
                                <div class="price_recommended">{{$single_habitation->price}}€</div>
                                <div class="description_recommended">
                                    {{ \Illuminate\Support\Str::words($single_habitation->desc_habitation, 12, ' ...')}}
                                </div>
                            </div>
                        @endforeach




                    </div>
                </div>
            </div>

        @endif
</div>
@stop
