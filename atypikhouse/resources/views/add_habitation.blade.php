@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')

    <div class="container add_habitation" id="add_habitation">
        <h1 class="text-center">Ajouter une location</h1>
        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Présentation</li>
            <li class="tab-link" data-tab="tab-2">Photos</li>
            <li class="tab-link" data-tab="tab-3">équipements</li>
            <li class="tab-link" data-tab="tab-4">Prix</li>
            <li class="tab-link" data-tab="tab-5">Lieu</li>
            <li class="tab-link" data-tab="tab-6">Disponibilité</li>
            <li class="tab-link" data-tab="tab-7">Activité</li>
        </ul>
        <form action="{{url('presentation')}}" method="post" id="presentation" enctype="multipart/form-data">
        <div id="tab-1" class="tab-content current">

            @csrf
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="titre_habitation" class="col-sm-3 col-form-label">titre habitation</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="titre_habitation" name="titre_habitation">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="desc_habitation" class="col-sm-3 col-form-label">description habitation</label>
                    <div class="col-sm-7">
                        <textarea name="desc_habitation" id="desc_habitation" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cat_habitation" class="col-sm-3 col-form-label">Catégorie generale</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="cat_habitation" name="cat_habitation">
                          @foreach ($categories as $cat)
                                <option value="{{$cat->id}}">{{$cat->title_categorie}}</option>

                            @endforeach

                        </select>
                    </div>
                </div>
                {{--<div class="form-group row">
                    <label for="type_habitation" class="col-sm-3 col-form-label">Type logement</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="type_habitation" name="type_habitation">
                            <option>Cabane</option>
                            <option>Yakht</option>
                        </select>
                    </div>
                </div>--}}

                <div class="form-group row">
                    <label for="chambre_habitation" class="col-sm-3 col-form-label">Nombre chambres</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="chambre_habitation"  name="chambre_habitation" >
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="condition_habitation" class="col-sm-3 col-form-label">Réglement & condition</label>
                    <div class="col-sm-7">
                        <textarea name="condition_habitation" id="condition_habitation" rows="3"></textarea>
                    </div>
                </div>


                <!--  </form>-->
            <!--  end form -->

        </div>
        <div id="tab-2" class="tab-content">

            <div class="card-body">
                <!-- <form  action="" enctype="multipart/form-data" class="dropzone" id="dropzone"
                      method="POST"> -->
                <div class="container_file">
                <input type="file" multiple="multiple" id="gallery-photo-add" name="photos_habitation[]" class="inputfile">
                <label for="gallery-photo-add">Uploder la photo</label>
                </div>
                <div class="gallery"></div>

                <!--  </form>-->
            </div>
        </div>
        <div id="tab-3" class="tab-content">
            <div class="equipements row ">
                {{--<form action="" class="form-row">--}}
                    <div class="col-6">

                    @foreach ($equipments as $eq)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input checkbox_equipement" id="{{$eq->id}}" name="equipments_list[]" value="{{$eq->name}}" data-equipement="{{$eq->id}}">
                               {{-- <input type="checkbox" class="custom-control-input" id="{{$eq->name}}" name="equipments_list_value[]" value="{{$eq->name}}" hidden="true">--}}
                                <label class="custom-control-label" for="{{$eq->id}}">{{$eq->name}}</label>
                            </div>
                            @endforeach


                    </div> <!-- end first block  -->

              </div>

          </div>
          <div id="tab-4" class="tab-content">
              <div class="price_habitaion_container">
                  {{-- <form>--}}
                      <div class="form-group row">
                          <label for="price_habitaion" class="col-sm-3 col-form-label  text-right">Prix TTC : </label>
                          <div class="col-sm-7">
                              <input type="text" class="form-control"  name="price_habitaion" id="price_habitaion" placeholder="€">
                          </div>
                      </div>
                  {{-- </form> --}}
              </div>
          </div>
          <div id="tab-5" class="tab-content">
              <div class="location_habitation_container">
                  <!-- <form action="">-->
                      {{ csrf_field() }}

                      <div class="form-group row">
                      <label for="pays" class="col-sm-3 col-form-label">Pays / Région</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="pays" name="pays">
                              <option>France</option>

                          </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="adresse_habitation" class="col-sm-3 col-form-label">Adresse</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" id="adresse_habitation" name="adresse_habitation">

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="postal_habitation" class="col-sm-3 col-form-label">Code postal</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" id="postal_habitation" name="postal_habitation">

                      </div>
                  </div>
                  <div class="form-group row">

                      <label for="ville_habitation" class="col-sm-3 col-form-label">Ville</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" id="ville_habitation" name="ville_habitation">
                      </div>

                  </div>
              <!--   </form>-->
              </div>
          </div>
          <div id="tab-6" class="tab-content">
              <div class="dispo_habitation">
                  <!--   <form action="">-->
                      {{ csrf_field() }}

                      <div class="form-group row">

                      <label for="date_debut_habitation" class="col-sm-3 col-form-label text-right">Date début</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" id="date_debut_habitation" name="date_debut_habitation" placeholder="Selectionner une date..">
                      </div>

                  </div>
                  <div class="form-group row">

                          <label for="date_fin_habitation" class="col-sm-3 col-form-label text-right">Date Fin</label>
                          <div class="col-sm-7">
                              <input type="text" class="form-control" id="date_fin_habitation" name="date_fin_habitation" placeholder="Selectionner une date..">
                          </div>

                      </div>
                      {{--<div class="form-group row">

                          <label for="desc_resevartion" class="col-sm-3 col-form-label text-right">Description</label>
                          <div class="col-sm-7">
                              <textarea name="desc_resevartion" id="desc_resevartion" rows="3"></textarea>
                          </div>

                      </div>--}}
              <!--  </form> -->
              </div>
          </div>
          <div id="tab-7" class="tab-content">
          <div class="container_activite_habitation">
          <!--  <form action="file-upload.php" class=" files-container">-->
              <div class="form-group row">


                  <label for="titre_activite" class="col-sm-3 col-form-label">titre activité</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" id="titre_activite"  name="titre_activite">

                  </div>
              </div>

              <div class="form-group row">
                  <label for="desc_activite" class="col-sm-3 col-form-label">Déscription activité</label>
                  <div class="col-sm-7">
                      <textarea name="desc_activite" id="desc_activite" rows="3"></textarea>
                  </div>
              </div>
              <div class="form-group row">


                  <label for="price_activite" class="col-sm-3 col-form-label">prix activité</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" id="price_activite"  name="price_activite">

                  </div>
              </div>
                  <div class="form-group row">
                      <label for="photo_activite" class="col-sm-3 col-form-label">Photos</label>
                      <div  class="col-sm-7">
                          <div class="container_file">
                              <input type="file" multiple id="gallery-photo-add2" id="gallery-photo-add2" name="photos_activitie[]" class="inputfile">
                              <label for="gallery-photo-add2">Uploder la photo</label>
                          </div>
                          <div class="gallery2"></div>
                    </div>
                </div>
           <!-- </form>-->
        </div>
        </div>
     <button type="submit" class="btn btn-primary" id="submit-all">Enregister</button>
        </form>
        <div class="errors_form">
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            @if (count($errors) > 0)

                <div class="alert alert-danger">

                    <strong>Whoops!</strong> There were some problems with your input.

                    <ul>

                        @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                        @endforeach

                    </ul>

                </div>

            @endif
        </div>
    </div>
    <!--  end container -->







@stop

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

@stop