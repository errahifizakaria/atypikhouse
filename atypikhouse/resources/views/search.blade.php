@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')







    <div class="recommended container">
        <h1 class="text-center"> Resultat </h1>
        <div class="row">
            <div class=" container" id="slick_search">

                @if(isset($result) && sizeof($result) > 0)

                    @if($result)
                    @foreach ($result as $row)
                        <div class="item_habitation">
                            <a href="/habitation/{{$row->id_habitation}}"> <img src="{{ asset('/images/photos_habitation/' . $row->photos . '') }}"  alt="{{$row->title_habitation}}"></a>
                            <div class="price_habitation">{{$row->title_habitation}}</div>
                            <div class="title_habitation">40 €</div>
                            <div class="description_habitation">

                                {{ \Illuminate\Support\Str::words($row->desc_habitation, 12, ' ...')}}

                            </div>
                        </div>
                    @endforeach
                    @endif

                @elseif(isset($single) && sizeof($single) > 0)
                    @if($single)
                    @foreach ($single as $row)



                        <div class="item_habitation">


                            <a href="/habitation/{{$row->id}}"> <img src="{{ asset('/images/photos_habitation/' . $row->photos . '') }}" alt="{{$row->title_habitation}}"></a>
                            <div class="price_habitation">{{$row->title_habitation}}</div>

                            <div class="description_habitation">

                                {{ \Illuminate\Support\Str::words($row->desc_habitation, 12, ' ...')}}

                            </div>
                        </div>
                    @endforeach
                    @endif
                @else
                    <h5 class=" text-center m-3 mt-4"> pas de resultat </h5>
                @endif




            </div>
        </div>
    </div>

@stop