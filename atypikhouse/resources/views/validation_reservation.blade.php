@extends('template.master')

@section('title', 'Atypikhouse')



@section('content')



    <div class="recommended container">
                <h1 class="text-center"> validation réservations. </h1>
        <div class="row">
            <div class=" validate_reservation" id="validate_reservation">

                @if(sizeof($reservation) > 0)
                    <form action="{{url('update_reservation')}}" method="post">
                @foreach ($reservation as $row)



                    <div class="item_habitation col-md-3 col-sm-12">
                        <a href="#"> <img src="{{asset("/img/recommended.png")}}" alt=""></a>
                        <div class="price_habitation">{{$row->title_habitation}}</div>

                            @csrf
                            <input type="hidden" value="{{$row->id_habitation}}" name="id_habitation">
                            <button type="button" class="btn btn-primary submit_validate_reservation">Valider</button>

                    </div>
                @endforeach
                    </form>
@endif



            </div>
        </div>
    </div>

@stop