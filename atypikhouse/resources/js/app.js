/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'slick-carousel'
import $ from 'jquery';
import 'typeahead.js';
const fancybox = require("@fancyapps/fancybox");
// And their styles (for UI plugins)

window.$ = window.jQuery = $;

const flatpickr = require("flatpickr");

const dmUploader = require("dm-file-uploader");
import { French } from "flatpickr/dist/l10n/fr.js";
import  '@fancyapps/fancybox/dist/jquery.fancybox.css';
import swal from 'sweetalert';
var moment = require('moment');
require('moment/locale/fr');
moment.updateLocale('fr', {
    /**/
});
moment.defineLocale('fr-foo', {
    parentLocale: 'fr',
    /* */
});
moment.updateLocale('fr', null);
// date birthday
flatpickr.localize(French); // default locale is now Russian

flatpickr("#birth",{
    "locale": French,
    minDate:"1950-01",
    dateFormat: "d/m/Y",

});
flatpickr("#user_birth",{
    "locale": French,
    minDate:"1950-01",
    dateFormat: "d/m/Y"

});



flatpickr(".flatpickr_date",{

    dateFormat: "d/m/Y",
    wrap: true, "locale": French,
    minDate: "today",
    mode: 'range',
});
// date_debut_habitation
flatpickr("#date_debut_habitation",{
    "locale": French,

    dateFormat: "d/m/Y"

});
flatpickr("#date_fin_habitation",{

    "locale": French,

    dateFormat: "d/m/Y"

});

$(document).ready(function(){

    $('.slick_activities').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        centerPadding: '60px',
        prevArrow: "<a href='#' class='slick_most_viseted_prev'></a>",
        nextArrow: "<a href='#' class='slick_most_viseted_next'></a>",
    });
    $('.slick_most_viseted').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        centerPadding: '60px',
        prevArrow: "<a href='#' class='slick_most_viseted_prev'></a>",
        nextArrow: "<a href='#' class='slick_most_viseted_next'></a>",
    });
    $('.slick_recommended').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: "<a href='#' class='slick_most_viseted_prev'></a>",
        nextArrow: "<a href='#' class='slick_most_viseted_next'></a>",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });


    var data_equipments ;


    $("form#presentation").submit(function (e) {
        var checkedVals = $('.checkbox_equipement:checkbox:checked').map(function() {
            return $(this).data("equipement");
        }).get();
        data_equipments = checkedVals.join(",");

        e.preventDefault();
        var form = e.currentTarget;
        var formData = new FormData($(this)[0]);
        formData.append("id_equipments",data_equipments);
        var files=[];

       for (var i = 0, len = document.querySelector('#gallery-photo-add').files.length; i < len; i++) {
           //files.push(document.querySelector('#gallery-photo-add').files[i])
           formData.append("file_habtation" + i, document.querySelector('#gallery-photo-add').files[i]);
        }
        //formData.append("file_habtation",files);
        /* for (var i = 0, len = document.querySelector('#gallery-photo-add2').files.length; i < len; i++) {
            formData.append("file" + i, document.querySelector('#gallery-photo-add2').files[i]);
        }*/

        //formData.append("file_habiation", document.querySelector('#gallery-photo-add2').files);
     $.ajax({

            url: '/presentation',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            data :formData

            ,success: function(data){

                if(jQuery.parseJSON(data)){
                        var resultat = jQuery.parseJSON(data);

                        if(resultat.error){
                           printErrorMsg(resultat.error);

                        }else{
                            $(".errors_form").fadeOut();
                            swal("Valider", "Bien enregister", "success");
                            //document.location.reload(true);

                        }


                }

            } //end success

        })

    }); // end submit
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img class="img_habitation_click">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });

    var imagesPreview2 = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add2').on('change', function() {
        imagesPreview2(this, 'div.gallery2');
    });

    var imagesPreviewUser = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).addClass("img_user").appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };
    $('#gallery-photo-add-user').on('change', function() {
        //$(".close_img").show();
        $(".profile-header-img").find("img").first().remove();
        imagesPreviewUser(this, '.profile-header-img');
    });

    $(".close_img").on('click',function () {
        $(this).next().remove();
        $(".close_img").hide();
    });
    // send request search by ajax
    $("form#search_form").submit(function (e) {
        //e.preventDefault();
        var data_search = $("#search_habitation").val();

      /*  $.ajax({

            url: '/search',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'post',
            data : { search_habitation : data_search}

            ,success: function(data){
               console.log(data);
            }

        })*/

    });

    $('#search_habitation').on('keyup',function(){
        var value=$(this).val();
        if($(this).val().length > 3){
            $.ajax({
                type : 'get',
                url : '/search',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{'search':value},
                success:function(data){
                    $('#result').html(data);
                }
            });
        }

    })
    function printErrorMsg (msg) {
        console.log(msg);
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            var matches = [];

            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    var states = ['Paris', 'Moscow', 'London', 'Madrid', 'London',
        'Berlin', 'Kiev', 'Bucharest', 'Barcelona', 'Bucharest', 'Vienna',
        'Munich'];


    $('#where').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'states',
           source: substringMatcher(states)

        });
    $(".account").click(function(e)
    {
        e.preventDefault();
        var X=$(this).attr('id');
        if(X==1)
        {
            $(".submenu").hide();
            $(this).attr('id', '0');
        }
        else
        {
            $(".submenu").show();
            $(this).attr('id', '1');
        }
    });
    $(".submenu").mouseup(function()
    {
        return false
    });
    $(".account").mouseup(function()
    {
        return false
    });
    $(document).mouseup(function()
    {
        $(".submenu").hide();
        $(".account").attr('id', '');
    });
    $(".link_update_info").on("click",function () {
        $(".form_two_update").hide();
        $(".form_one_update").show();
    });
    $(".link_update_photo").on("click",function () {
        $(".form_one_update").hide();
        $(".form_two_update").show();
    });
    $("#submit_reservation_habiation").on("click",function (e) {

        e.preventDefault();

        var date_reservation = $("input[name=date_reservation]").val();

        var id_habitation = $('.content_habitation').data("id");

        var nombre_voyageur = $("select[name=nombre_voyageur]").val();
        var total = parseInt($(".price_cart_total").text());
        var days_reservation = $(".days_reservation").text();


         $.ajax({
         headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'POST',

         url:'/detailReseravation',

         data: {id_habitation:id_habitation,date_reservation:date_reservation,nombre_voyageur:nombre_voyageur,total:total,days_reservation:days_reservation},

         success:function(data){
            console.log("success",data);
        if(data.success)  {
            swal({
                title: '',
                text: 'réservation effectuée.',
                icon: 'success',
                timer: 2000,
                buttons: false,
            }).then(() => {
                dispatch(redirect('/'));

        });
            document.location.href = "/paiement"
        }


             //document.location.href = document.location.href;

         },error:function (data) {

                 $.each(data.responseJSON.errors, function (key, item)
                 {
                     console.log(item);

                         $(".errors_reservation ul ").append("<li class='custom_error'>" + item + "</li>");



                 });
                 if(data.responseJSON.message =="Unauthenticated.")  $('#modal').modal('show');
         }

         });
        $(".errors_reservation ul ").html('');
    });
/*
* validation resravation
*/
    $(".submit_validate_reservation").on("click",function (e) {

        e.preventDefault();
//$("input[name=id_habitation]").val();
        var id_habitation = $(this).parent().find('input[name=id_habitation]').val();

console.log(id_habitation);


        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',

            url:'/update_reservation',

            data: {id_habitation:id_habitation},

            success:function(data){

               if(data.success) {
                   swal({
                       title: '',
                       text: 'réservation confirmé.',
                       icon: 'success',
                       timer: 2000,
                       buttons: false,
                   }).then(() => {
                       dispatch(redirect('/'));

               });

                   document.location.href = "/validate"
               }

            }

        });

    });
    /*
     * send contact form
     */
    $("#form_contact_owner_submit").on('click',function (e) {

        e.preventDefault();

        var txt_form_contact_owner = $("#txt_form_contact_owner").val();

            var data_form = $('#form_contact_owner').serialize();



        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',

            url:'/contact_owner',

            data: data_form,

            success:function(data){
                console.log(data);
                if(data.success == "1"){
                    swal({
                        title: '',
                        text: "Message envoyé",
                        icon: 'success',
                        timer: 2000,
                        buttons: false,
                    }).then(() => {
                        dispatch(redirect('/'));
                })
                    $('#contact_owner').modal('hide');
                    $('.modal-backdrop').hide();
                }


            },
            error:function(results){
                if(results.responseJSON.errors){
                 $(".error_txt_form_contact_owner").html("<p class='text-center alert alert-danger mt-3' role='alert'>"+ results.responseJSON.errors.txt_form_contact_owner +"</p>");
                 }


            }

        });

    });
    // send date to create new register submit_regiter
    $("#submit_regiter").on('click',function (e) {

        e.preventDefault();

        var data = $("#regiter_form").serialize();





        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',

            url:'/register',

            data: data,

            success:function(data){

                $('#registerModal').modal('hide');
                $('.modal-backdrop').hide();
                document.location.href="/"


            },


        });

    });

    $(".add_location").click( function(){
        $('#modal').modal('show');
    });
    $("input[name=isOwner]").click( function(){

        if( $(this).is(':checked') ) $("#siret").show();
        else $("#siret").hide();
    });
    var current_price = $('.price_cart_total').text();
$("#date_reservation").on('change',function(e){

    var value = $(this).val();

    $('.price_cart_total').text('');

    if(value.length > 10){

        var dates = value.split("au");
        var start_value =  $.trim(dates[0]);
        var end_value =  $.trim(dates[1]);
        var star = start_value.split("-");
        var end = end_value.split("-");

        var date_start = new Date(star[2],star[1],star[0]);
        var date_end = new Date(end[2],end[1],end[0]);

        var a = moment([end[2], end[1], end[0]]);
        var b = moment([star[2], star[1], star[0]]);
      var days = a.diff(b, 'days');
      // get days_reservation in dom

      $('.days_reservation').text(days);




      $('.price_cart_total').html(days * parseInt(current_price.trim()));


    }else {

       // $('.price_cart_total').text("");
    }

});
}); // end ready function


