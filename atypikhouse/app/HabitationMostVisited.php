<?php

namespace App;
use App\Habitation as Habitation;
use Illuminate\Database\Eloquent\Model;


class HabitationMostVisited extends Model
{
    protected $fillable=['id_habitation'];
    protected $table="habitation_most_visited";


    public function habitationId()
    {
        return $this->belongsTo('App\Habitation');


    }

    public function habitation()
    {
        return $this->belongsTo('App\Habitation');
    }
}
