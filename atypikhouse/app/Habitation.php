<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Habitation extends Model
{

    protected $fillable = [
        'title_habitation', 'desc_habitation','adresse_habitation','photos','regles'
    ];
    /**
     * Get the features record associated with the habitation.
     */
    public function feature()
    {
        return $this->hasOne('App\Feature',"id_habitation");
    }

    /**
     * Get the reservation for the habitation.
     */
    public function reservations()
    {
        return $this->hasMany('App\reservation','id_habitation');
    }

    public function categorie()
    {
        return $this->belongsTo('App\Categorie');
    }
    public function HabitationMostVisited()
    {
        return $this->belongsTo('App\HabitationMostVisited');
    }
    public function HabitationRecommended()
    {
        return $this->belongsTo('App\HabitationRecommended');
    }

    /**
     * Get the acitivity record associated with the habitation.
     */
    public function activity()
    {
        return $this->hasOne('App\Activitie',"id_habitation");
    }

    public function disponiblite()
    {
        return $this->hasOne('App\Disponibilite','id_habitation');
    }

}
