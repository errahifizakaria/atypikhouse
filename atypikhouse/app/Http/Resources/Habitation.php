<?php

namespace App\Http\Resources;
use App\Http\Resources\Equipment as EquipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Equipment;
class Habitation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
<<<<<<< HEAD
     * @param  \Illuminate\Http\Request  $request
=======
     * @param \Illuminate\Http\Request $request
>>>>>>> dfc9f6a9fb461ab0ab5e5d6e1ad506d4f380ac2f
     * @return array
     */
    public function toArray($request)
    {
        $equipments = Equipment::paginate(15);

        return [
            'id' => $this->id,
            'id_user' => $this->id_user,
            'title_habitation' => $this->title_habitation,
            'desc_habitation' => $this->desc_habitation,
            'adresse_habitation' => $this->adresse_habitation,
            'photos' => $this->photos,
            'price' => $this->feature->price,
            'equipment' => EquipmentResource::collection($equipments),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

    }
}
