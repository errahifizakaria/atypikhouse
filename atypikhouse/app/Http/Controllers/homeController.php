<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\HabitationMostVisited;
use App\HabitationRecommended;
use App\Habitation;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = DB::table('categories')->get();
        $equipments = DB::table('equipments')->get();
        //$habitation_most_viewed = HabitationMostVisited::with('habitation')->where('active','=',1)->distinct()->limit(10)->get();
        //$habitation_recommended = HabitationRecommended::with(['habitation'])->where('active','=',1)->distinct()->limit(10)->get();
        $habitation_recommended = DB::table('HabitationRecommended')
            ->join('habitations', 'HabitationRecommended.habitation_id', '=', 'habitations.id')
            ->join('features', 'habitations.id', '=', 'features.id_habitation')
            ->select('habitations.*', 'features.*')
            ->where('HabitationRecommended.active','=',1)
            ->get()
        ;

        $habitation_most_viewed = DB::table('habitation_most_visited')
            ->join('habitations', 'habitation_most_visited.habitation_id', '=', 'habitations.id')
            ->join('features', 'habitations.id', '=', 'features.id_habitation')
            ->select('habitations.*', 'features.*')
            ->where('habitation_most_visited.active','=',1)
            ->get()
        ;
        $last_habitation = Habitation::with('feature')->orderBy('created_at', 'DESC')->get();



        return view('home', ['categories' => $categories,"equipments" => $equipments,'habitation_most_viewed' => $habitation_most_viewed,'habitation_recommended' => $habitation_recommended , 'last_habitation'=> $last_habitation]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $categories = DB::table('categories')->get();
        $equipments = DB::table('equipments')->get();
        $habitation_most_viewed = DB::table('habitation_most_visited')->distinct()->where('active','=',1)->get();
        $habitation_recommended = DB::table('HabitationRecommended')->distinct()->where('active','=',1)->get();

        return view('welcome', ['categories' => $categories,"equipments" => $equipments,'habitation_most_viewed' => $habitation_most_viewed,'habitation_recommended' => $habitation_recommended ]);
    }



}
