<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;
use Mockery\Exception;
use Illuminate\Support\Facades\Auth;

class MailController extends Controller
{

    public function basic_email( Request $request) {

        if ($request->ajax()){
            $messages = [
                'required' => 'Le message vide.',
            ];
            $request->validate([
                'message_to_owner' => 'required',
                'subjet' => 'required',
            ], $messages);

        $msg = $request->input("message_to_owner");
        $subjet = $request->input("subjet");
        $owner_email= $request->input("owner_email");
        $user_guest_email = $request->input("user_guest");
        $from = 'contact@atypikhouse.site';
        $data = array('msg'=>$msg);

        try{
            if(Auth::guest())
            {
                Mail::send('mail', $data, function($message) use ($subjet,$user_guest_email,$from,$owner_email) {
                    $message->to($owner_email)
                        ->subject($subjet);
                    $message->from($user_guest_email);
                });
            }else{
                Mail::send('mail', $data, function($message) use ($subjet,$from,$owner_email) {
                    $message->to($owner_email)
                        ->subject($subjet);
                    $message->from(Auth::user()->email);
                });
            }


        }catch (Exception $exception){
            echo $exception->getMessage();
        }

            return response()->json(['success' => '1']);


        }
    }
}
