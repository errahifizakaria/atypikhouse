<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mockery\Exception;

class SearchController extends Controller
{

    /**
     * get result search .
     *
     *
     */
    public function searchHome(Request $request)
    {
        $full_adress = DB::table('habitations')->pluck('adresse_habitation');
        $full_adress = explode(',',$full_adress);

        $adress = $full_adress[0];
        $cp = $full_adress[1];
        $city = strtolower($full_adress[2]);
        $contry = $full_adress[2];

        if(strlen($request["where"]) > 0 ){  $query = strtolower($request["where"]); }
        $results="";
        if($request->has('type_location') && $request->has('date_reservation') && !empty($request["date_reservation"]) ) {



            $type_location = $request["type_location"];
        $date_reservation_data = $request["date_reservation"];
        $date_reservation = explode('au',$date_reservation_data);

        $date_begin = date("Y-m-d", strtotime(str_replace('/', '-', trim($date_reservation[0]))));


       $date_end = date("Y-m-d", strtotime(str_replace('/', '-', trim($date_reservation[1]))));




            /* if ($query == $city) {
                  $results = DB::table('habitations')->where('adresse_habitation', 'LIKE', '%' . $query . "%")->get();
              }*/

        try {

            $results =$this->searchBy($type_location,$date_begin,$date_end);
            //dd(trim($date_reservation[0]),$date_begin,trim($date_reservation[1]),$date_end);

        } catch (Exception $exception) {

            echo "Pas de resultat...";

        }
            return view('search',["result" => $results]);
        } else if($request->has('where') && strlen($request["where"]) > 0){

            $results = DB::table('habitations')->where('adresse_habitation', 'LIKE', '%' . $query . "%")->get();
            return view('search',["single" => $results]);
        }

        return view('search');

    }


    /*
         * function for searching habitation
         */
    public function searchHabitation(Request $request)
    {

        //return view("search");

        if ($request->ajax()) {
            $query = $request["search"];

            $output = "";
            $results = DB::table('habitations')->where('title_habitation', 'LIKE', '%' . $query . "%")->get();
            if ($results) {
                $output .= '<ul>';
                foreach ($results as $key => $row) {
                    $output .=
                        '<li> <a href="/habitation/'.$row->id.'">' . $row->title_habitation . '</a></li>';


                }
                $output .= '</ul>';
                return Response($output);


            }


        }


    }// end function searchHabitation

    /*
     * return result of search query
     */
    function searchBy($type=null,$date_begin=null,$date_end=null,$available=1){


         /* $results = DB::table('habitations')->where([
            'title_habitation', '=', $title,


        ])->get();*/
//        ->join('contacts', 'users.id', '=', 'contacts.user_id');

        $results = DB::table('habitations')
            ->join('categories', 'habitations.category_id', '=', 'categories.id')
            ->join('reservations', 'habitations.id', '=', 'reservations.id_habitation')
         ->select('habitations.*', 'categories.*','reservations.*')
          ->where([

                ['categories.title_categorie', '=', $type],
             ['reservations.date_begin', '>=', $date_begin],
              ['reservations.date_end', '<=', $date_end],
            ])
            ->get();
        return $results;

    }
    /*
     * search top
     */
    function searchTop(Request $request){
        $query = $request->query("search_habitation") ;
        if ($query && strlen($query) > 0) {
            $single = DB::table('habitations')->where('title_habitation', 'LIKE', '%' .$query. "%")->get();

            //var_dump($result);
            return view("search", ["single" => $single]);
        }

    }
}
