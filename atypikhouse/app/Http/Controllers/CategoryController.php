<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\HabitationMostVisited;
use App\HabitationRecommended;
use App\Habitation;
class CategoryController extends Controller
{

    /*
     * return habiation bu catgeory
     */
    public function show(Request $request, $id)
    {

        $habitation_category = Habitation::with('feature')->where('category_id','=',$id)->orderBy('created_at', 'ASC')->get();

         return view('category', ['habitation_category' => $habitation_category]);


    }





}
