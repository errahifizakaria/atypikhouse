<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class reservationController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $habitation = DB::table('habitations')->where("id_user","=",$user_id)->get();
        $reservation = DB::table('reservations')->where("id_user","=",$user_id)->get("id_habitation");
        $features = DB::table('features')->where("id_user","=",$reservation)->get();


        return view('reservation_histroy',["reservation"=>$reservation,"habitation"=>$habitation,"features"=>$features]);

    }

    public function validate_reservation()
    {


        $user_id = Auth::id();
        if( Auth::user()->is_owner){


       /* $habitation = DB::table('habitations')->where("id_user","=",Auth::user()->id)->get();
        $reservation = DB::table('reservations')

            ->where("statut","=","en attente")
            ->get("id_habitation");*/
            $reservation_to_validate = DB::table('habitations')
                ->join('reservations', 'reservations.id_habitation', '=', 'habitations.id')
                ->where("habitations.id_user","=",Auth::user()->id)
                ->where("reservations.statut","=","en attente")

                ->select('habitations.*', 'reservations.*')
                ->get();


        return view('validation_reservation',["reservation"=>$reservation_to_validate]);

        }

    }
    public function update_reservation(Request $request)
    {

        if ($request->ajax()) {

            $id_habitation_to_update = intval($request->input('id_habitation'));

            DB::table('reservations')
                ->where('id_habitation', $id_habitation_to_update)
                ->update(['statut' => 'confirmé']);
            return response()->json(['success' => '1']);
        }
    }


    public function history_reservation()
    {
        $user_id = Auth::id();
        $reservation_to_validate = DB::table('habitations')
            ->join('reservations', 'reservations.id_habitation', '=', 'habitations.id')
            ->where("reservations.id_user","=",$user_id)

            ->select('habitations.*', 'reservations.*')
            ->get();

        return view('history_reservation',["reservation"=>$reservation_to_validate]);

    }
}
