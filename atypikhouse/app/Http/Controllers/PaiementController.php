<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;


class PaiementController extends Controller
{

    public function pay( Request $request) {
        $total = $request->session()->get("total");
        \Stripe\Stripe::setApiKey('sk_test_MDHoWOyNsIKlOOMzLVl1bu7K00IQb7EKTV');
        try {
            \Stripe\Charge::create(array(
                "amount" => $total * 100,
                "currency" => "eur",
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => "Test payment."
            ));
            Session::flash('success-message', 'Payment done successfully !');
            return \Redirect::back();
        } catch (\Exception $e) {
            Session::flash('fail-message', "Error! Please Try again.");
            return \Redirect::back();
        }
    }
}
