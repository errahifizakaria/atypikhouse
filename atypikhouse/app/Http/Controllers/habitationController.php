<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Habitation;
use App\Categorie;
use App\Comment;
use App\Equipment;
use App\Feature;
use App\Reservation;
use App\View;
use App\User;
use App\Disponibilite;
use App\Activitie;
use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Http\Resources\Habitation as HabitationResource;

class habitationController extends Controller
{


    /*
     * show habitation
     */
    function showHabitation()
    {
        $categories = DB::table('categories')->get();
        $equipments = DB::table('equipments')->get();

        //dd($categories);

        return view("add_habitation", ['categories' => $categories, "equipments" => $equipments]);
    }


    public function presentation(Request $request)
    {
        /**
         * validation
         */


        $rules = array(
            'titre_habitation' => 'required',
            'adresse_habitation' => 'required',
            'postal_habitation' => 'required',
            'ville_habitation' => 'required',
            'pays' => 'required',
            'id_equipments' => 'required',
            'price_habitaion' => 'required',
            'chambre_habitation' => 'required',
            'date_debut_habitation' => 'required',
            'date_fin_habitation' => 'required',
            'titre_activite' => 'required',
            'desc_activite' => 'required',
            'price_activite' => 'required',
            'photos_activitie' => 'required',
        );
        $messages =  array(
            'required' => 'required',
        );
        $photos_habitation_count = count($request->file("photos_habitation"));
        $photos_activitie_count = count($request->file("photos_activitie"));

        foreach (range(0, $photos_habitation_count) as $index) {
            $rules['photos_habitation.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }
        foreach (range(0, $photos_activitie_count) as $index) {
            $rules['photos_activitie_count.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }

        $validator = Validator::make($request->all(),$rules , $messages);

        // use ajax form
        if ($request->ajax()) {
            // if user is login
            if (Auth::check()) {

                $id_user = Auth::id();

                $adresse_habitation = $request["adresse_habitation"] .",". $request["postal_habitation"] .",". $request["ville_habitation"] .",". $request["pays"];

                if ($validator->passes()) {
                    $id_habitation = $this->addHabitation($id_user, $request["titre_habitation"], $request["cat_habitation"], $request["desc_habitation"], $request["condition_habitation"], $adresse_habitation, $request->file('photos_habitation'));

                    $this->addFeature($id_habitation, $request["id_equipments"], $request["chambre_habitaion"], $request["price_habitaion"]);
                    $this->addDisponiblite($request["date_debut_habitation"], $request["date_fin_habitation"], $id_habitation);
                    //$this->addReservation($id_habitation, $id_user, $request["date_debut_habitation"], $request["date_fin_habitation"]);
                    $this->addActivity($id_habitation, $request["titre_activite"], $request["price_activite"], $request->file('photos_activitie'));

                    return response()->json(['success' => '1']);
                }
                return response()->json(['error' => $validator->errors()->all()]);


            }
        }

    } // end function presentation


    /*
     * add new habitation
     * return id_habitation
     */
    public function addHabitation($id_user, $title_habitation, $id_categorie, $desc_habitation, $condition, $adresse_habitation, $photos_habitation_save)
    {
        $newHabitation = new Habitation();

        $newHabitation->id_user = $id_user;
        $newHabitation->category_id = (int)$id_categorie;
        $newHabitation->title_habitation = $title_habitation;
        $newHabitation->desc_habitation = $desc_habitation;
        $newHabitation->regles = $condition;
        $newHabitation->available = 1;
        $newHabitation->adresse_habitation = $adresse_habitation;
        $photos_habitation = array();


        foreach ($photos_habitation_save as $photo) {
            $name = str_replace(' ', '_', $title_habitation.$photo->getClientOriginalName());

            $photo->move(public_path() . '/images/photos_habitation', $name);
            array_push($photos_habitation, $name);
        }


        $newHabitation->photos = implode(', ', $photos_habitation);
        $newHabitation->save();
        return $newHabitation->id;
    }

    /*
     * available habiation
     */

    function addDisponiblite($date_debut, $date_fin, $id_habitiation)
    {
        $newDisponiblite = new Disponibilite();

        $newDisponiblite->date_debut = Carbon::parse(strtotime(strtr($date_debut, '/', '-')))->format("Y-m-d");
        $newDisponiblite->date_fin = Carbon::parse(strtotime(strtr($date_fin, '/', '-')))->format("Y-m-d");
        $newDisponiblite->id_habitation = $id_habitiation;
        $newDisponiblite->save();

    }

    /*
    * add new feature
    */
    public function addFeature($id_habitation, $id_equipments, $chambre_habitation, $price_habitaion)
    {
        $newFeature = new Feature();
        // $list = explode(",",$id_equipments);
        $newFeature->id_habitation = $id_habitation;
        $newFeature->id_equipment = $id_equipments;
        $newFeature->rooms = $chambre_habitation;
        $newFeature->price = $price_habitaion;
        $newFeature->save();
        /* foreach ($id_equipments as $equipement) {

             $newFeature->id_habitation = $id_habitation;
             $newFeature->id_equipment = $equipement;
             $newFeature->save();
         }*/

    }

    /*
     * add new reservaton
    */
    public function addReservation($id_habitation, $id_user, $date_begin, $date_end, $count_voyager = 1)
    {
        $newReservation = new Reservation();
        $newReservation->id_habitation = $id_habitation;
        $newReservation->id_user = $id_user;

        $newReservation->date_begin = Carbon::parse(strtotime(strtr($date_begin, '/', '-')))->format("Y-m-d");
        $newReservation->date_end = Carbon::parse(strtotime(strtr($date_end, '/', '-')))->format("Y-m-d");
        $newReservation->statut = "en attente";
        $newReservation->count_voyager = $count_voyager;
        $newReservation->save();

    }

    /*
     * add new activity
     */

    public function addActivity($id_habitation, $title_activity, $price, $photos_activities_save)
    {
        $newActivitie = new Activitie();

        $newActivitie->id_habitation = $id_habitation;
        $newActivitie->title_activity = $title_activity;
        $newActivitie->distance = "";
        $newActivitie->price = doubleval($price);
        $photos_activities = array();


        foreach ($photos_activities_save as $photo_activitie) {
            $name_photos_activitie = str_replace(' ', '_', $photo_activitie->getClientOriginalName());
            $photo_activitie->move(public_path() . '/images/photos_activitie', $name_photos_activitie);

            array_push($photos_activities, $name_photos_activitie);

        }
        $newActivitie->photos_activity = implode(', ', $photos_activities);
        $newActivitie->save();

    }

    /*
     * Api
     * return habitation
     */
    public function show()
    {
        $habitation = Habitation::with('feature')->paginate(15);
        $equipments = Equipment::paginate(15);
        //$equipments = DB::table('equipments')->get();

        return HabitationResource::collection($habitation);
    }

    /*
     * detail habitation
     */
    public function detailHabitation(Request $request, $id)
    {

        $habitation = Habitation::findOrFail($id);

        $disponiblite = Habitation::find($id)->disponiblite()->orderBy("created_at", "DESC")->first();
        $id_user = Habitation::find($id)->first('id_user');
        if ($disponiblite !== null) {
            $activities = Habitation::find($id)->activity()->get();
            $featured = Habitation::find($id)->feature()->get();
            $equipments = Feature::find($featured[0]->id)->equipment()->get();
            $owner = User::find($id_user['id_user'])->first();
            //$equipment = Equipment::find((int)$featured[0]->id_equipment)->featured()->get();
            return view("detail_habitation", ['habitation' => $habitation, 'featured' => $featured, 'equipments' => $equipments, 'disponiblite' => $disponiblite, 'activities' => $activities, 'id_user' => $id_user, 'owner' => $owner]);
        } else {
            return Redirect::back();
        }

    }

    public function detailReseravation(Request $request)
    {
         $rules = array(
             'date_reservation' => 'required|min:24',
             'nombre_voyageur' => 'required|max:1',

         );
        $messages = array(
            'required' => 'Le champ :attribute est obligatoire',
            'max' => 'Le champ :attribute est obligatoire',
            'min' => 'Le champ :attribute est obligatoire',

        );
        $this->validate($request, $rules, $messages);



        if ($request->ajax()) {

            if (Auth::check()) {
                if (!empty($request['id_habitation']) > 0 && !empty($request['date_reservation']) > 0 && !empty($request['nombre_voyageur']) > 0) {
                    $date = explode('au', trim(str_replace("/", "-", $request['date_reservation'])));
                    $habitation = Habitation::findOrFail(intval($request['id_habitation']))->first();
                    $reservation = Reservation::where("id_habitation","=",intval($request['id_habitation']))->first();


                   if(count($reservation) > 0){

                   }else{

                       $this->addReservation(intval($request['id_habitation']), Auth::id(), date("Y-m-d", strtotime($date[0])), date("Y-m-d", strtotime($date[1])), intval($request['nombre_voyageur']));
                       $request->session()->put('nombre_voyageur', intval($request['nombre_voyageur']));
                       $request->session()->put('total', intval($request['total']));

                       return response()->json(['success' => '1']);
                   }






                    }




                }
            }

        }



} // end class

