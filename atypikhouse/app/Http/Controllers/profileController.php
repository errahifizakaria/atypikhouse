<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class profileController extends Controller
{

    /*
     * show profile user
     */
    public function showProfile(Request $request){

        $user = Auth::user();
        return view("profile",compact('user',$user));
    }
        /*
         * edit profile user
         */
    public function EditProfile(User $user,Request $request){
        // users
        $id_user =  Auth::id();
        $user =  Auth::user();
      $data = $request->validate([
            'name' => 'required',
            'email' => 'email',

            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
       //     'password_confirmation' => 'min:6',
        ]);

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        $request->avatar->storeAs('public/users',$avatarName);
        $user->fill($data);

        $user->avatar = $avatarName;

        $user->save();

        return back()
            ->with('success','You have successfully upload image.');

       // dd($request->all());

    }

}
