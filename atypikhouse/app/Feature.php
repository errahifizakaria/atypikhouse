<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{

    protected $fillable = [
        'rooms', 'price','tax'
    ];
    /**
     * Get the habitation of user.
     */
    public function habitation()
    {
        return $this->belongsTo('App\Habitation');
    }

    public function equipment()
    {
        return $this->belongsTo('App\Equipment','id_equipment');
    }
}
