<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HabitationRecommended extends Model
{

    protected $fillable=['id_habitation'];
    protected $table= "HabitationRecommended";
    public function habitationId()
    {
        return $this->belongsTo('App\Habitation');


    }
    public function habitation()
    {
        return $this->belongsTo('App\Habitation');
    }
    public function feature()
    {
        return $this->belongsTo('App\Feature');
    }
}
