<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    protected $fillable = [
        'id_habitation', 'id_user','date_begin','date_end','statut'
    ];

}
