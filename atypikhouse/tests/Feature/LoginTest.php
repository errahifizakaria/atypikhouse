<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Session;
class LoginTest extends TestCase
{


    /**
     *  test link login user exist
     */
    public function testLoginUserexist()
    {

        Session::start();
        $user = factory(User::class)->create([
            'password' => bcrypt($password = 'zak1234'),
        ]);
        $response = $this->post('/login', [
            'email' => 'errahifizakaria@gmail.com',
            'password' => $password,
            '_token' => csrf_token()
        ]);
        $response->assertRedirect('/home');
        $this->assertAuthenticated();



    }
    /**
     * test login new user
     */

    public function testLoginNewUser()
    {

        Session::start();
        $user = factory(User::class)->create([
            'password' => bcrypt($password = 'test'),
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
            '_token' => csrf_token()
        ]);
        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);



    }
    /** @test user register owner*/
    public function testRegister_creates_and_authenticates_a_user_owner()
    {
        //'name', 'email', 'password','tel','adresse','birth','avatar','is_owner','siret'
        Session::start();
        $response = $this->post('register', [
            'name' => 'Ramie2',
            'email' => 'jmac@example2.com',
            'password' => 'password',
            'tel' => '',
            'adresse' => '',
            'birth' => '1990-03-01',
            'avatar' => 'users/default.png',
            'is_owner' => '1',
            'siret' => '12345789',
            '_token' => csrf_token()

        ]);


        $response->assertRedirect('/home');
        $this->assertAuthenticated();

        // ...
    }
    /*
     * test json habitations response
     */
    public function testHabitationJsonResponse(){
        $response = $this->json('GET', 'habitations');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                "title_habitation"=> "La Cabane-Spa"
            ]);
    }
}
